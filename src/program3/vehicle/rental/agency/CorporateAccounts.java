//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class CorporateAccounts
//------------------------------------------------------------------------------------------
//CorporateAccounts stores the complete set of Account objects.
//******************************************************************************************
//List of methods: add, genAcctNum, nextFreeLoc, getAccounts
//******************************************************************************************
//==========================================================================================
public class CorporateAccounts {
    //--------------------------------------------------------------------------------------
    //Instance Variables
    //--------------------------------------------------------------------------------------
    private Account[] accounts;
    private static int latest_acct_num = 1; // init as first account number (i.e., 00001)
    
    //--------------------------------------------------------------------------------------
    //Constructor 
    //--------------------------------------------------------------------------------------
    public CorporateAccounts() {
	// allocate for max of 50 accounts
	accounts = new Account[50];

	// init as empty set of accounts
	for(int i = 0; i < 50; i++)
            accounts[i] = null;
    }//Close Constructor 
    
    //--------------------------------------------------------------------------------------
    //add 
    //--------------------------------------------------------------------------------------
    //Adds a new Account object to Accounts instance array 
    //--------------------------------------------------------------------------------------
    public String add(String name, boolean insur_desired) { 
        String acct_num = genAcctNum(); 
	accounts[nextFreeLoc()] = new Account(name, acct_num, insur_desired);
        return acct_num;
    }//Close add
    
    //--------------------------------------------------------------------------------------
    //genAcctNum
    //--------------------------------------------------------------------------------------
    //Automatically generate new account number
    //--------------------------------------------------------------------------------------
    private String genAcctNum() {
	String padded_zeros = "00000";
	String acct_num_str;

	// increment to next acct number
	latest_acct_num = latest_acct_num + 1;

	// convert int acct_num to String type
	acct_num_str = Integer.toString(latest_acct_num);

	// pad the acct_num with leading zeros so that of length 5
	acct_num_str =  padded_zeros.substring(acct_num_str.length()) + acct_num_str;
	
	return acct_num_str;
    }//Close genAcctNum
    
    //--------------------------------------------------------------------------------------
    //nextFreeLoc
    //--------------------------------------------------------------------------------------
    //Returns index of next free location in array accounts
    //--------------------------------------------------------------------------------------
    private int nextFreeLoc() {
	  
        for (int i = 0; i < 50; i++)
            if (accounts[i] == null)
		return i;
        return -1; 
    }//Close nextFreeLoc
    
    //--------------------------------------------------------------------------------------
    //Getter
    //--------------------------------------------------------------------------------------
    public Account getAccount (String acct_num) throws AccountException { 
        AccountsIterator itr = new AllAccountsIterator(accounts);
        Account[]array = new Account[50]; 
        int i = 0; 
      
        while (itr.hasNext()) { 
            if (i < 50) { 
                array[0] = itr.next(); 
                
                if (array[i].getAcctNum().equals(acct_num))
                    return array[i]; 
            }
                
            
           
            
            
            
            
            /*if (itr.next().getAcctNum().equals(acct_num)) {
               System.out.println (itr.next().getAcctNum());
               return itr.next(); 
           }
           itr.next();
*/
        }//Close while
        throw new AccountException (); 
    }//Close getVehicle
    //--------------------------------------------------------------------------------------
    
    //--------------------------------------------------------------------------------------
    //getAllAccountsItr
    //--------------------------------------------------------------------------------------
    public AccountsIterator getAllAccountsItr () { 
        return new AllAccountsIterator (accounts);
    }
    //--------------------------------------------------------------------------------------
}//Close CorporateAccounts Class
