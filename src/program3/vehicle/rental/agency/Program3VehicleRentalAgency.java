//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class Program3VehicleRentalAgency
//------------------------------------------------------------------------------------------
//Program3VehicleRentalAgency allows the user to pick the private customer UI, the corporate 
//customer ui, or the manger ui. 
//******************************************************************************************
//List of methods: main
//******************************************************************************************
//==========================================================================================
import java.util.Scanner; 
public class Program3VehicleRentalAgency {

    public static void main(String[] args) {
         Scanner input = new Scanner (System.in); 
         int selection; 
         SystemInterface.initSystem();
         

         System.out.print ("Select user type (1: Private Customer, 2: Corporate Customer, 3: Manager): "); 
         selection = input.nextInt(); 
         
         if (selection == 1) {
             System.out.println ("*****************************************************");
             System.out.println ("  * Welcome to the Friendly Vehicle Rental Agency *"); 
             System.out.println ("                  (Private Customer)                  ");
             PrivateCustomerUI ui = new PrivateCustomerUI();
             ui.start();      
         } 
         
         else if (selection == 2) { 
             System.out.println ("*****************************************************");
             System.out.println ("  * Welcome to the Friendly Vehicle Rental Agency *"); 
             System.out.println ("                (Corporate Customer)                  ");
             CorporateUserInterface ui = new CorporateUserInterface(); 
             ui.start(); 
         }
         
         else {
             System.out.println ("*****************************************************");
             System.out.println ("  * Welcome to the Friendly Vehicle Rental Agency *"); 
             System.out.println ("                      (Manager)                  ");
             ManagerUserInterface ui = new ManagerUserInterface(); 
             ui.start(); 
         }
                        
                
    }//Close main 
            
}//Close class

    

