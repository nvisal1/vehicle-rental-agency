//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class AgencyRates
//------------------------------------------------------------------------------------------
//AgencyRates assists the program in finding rates for all vehicle types
//******************************************************************************************
//List of methods: getCarRate, getSUVRate, getTruckRate
//******************************************************************************************
//==========================================================================================
public class AgencyRates {
    //--------------------------------------------------------------------------------------
    //Instance Variables
    //--------------------------------------------------------------------------------------
    private CarRates car_rate;
    private SUVRates suv_rate;
    private TruckRates truck_rate;
    
    //--------------------------------------------------------------------------------------
    //Constructor
    //--------------------------------------------------------------------------------------
    public AgencyRates() {
        car_rate = new CarRates();
        suv_rate = new SUVRates();
        truck_rate = new TruckRates();
    }//Close Constructor
   
    //--------------------------------------------------------------------------------------
    //Getters
    //--------------------------------------------------------------------------------------
    public Rates getCarRate(){
        return car_rate;
    }//Close getCarRate
    
    public Rates getSUVRate(){
        return suv_rate;
    }//Close getSUVRate
    
    public Rates getTruckRate(){
        return truck_rate;
    }//Close getTruckRate
    //--------------------------------------------------------------------------------------
}//Close AgencyRates Class
