//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class Vehicle
//------------------------------------------------------------------------------------------
//Vehicle stores the basic vehicle information (descript, mpg, num_value, and VIN). 
//The num_value means something different for each vehicle type. 
//There are also two other variables in a vehicle object, Reservation type and Cost type.
//******************************************************************************************
//List of methods: toString, getDescript, getMpg, getNumValue, getVIN, isReserved, getCost, setCost, 
//getReserv, setReserv, calcCost
//******************************************************************************************
//==========================================================================================
public abstract class Vehicle {
    //--------------------------------------------------------------------------------------
    //Instance Variables
    //--------------------------------------------------------------------------------------
    //basic vehicle information
    private String descript; 
    private int mpg; 
    private String num_value; 
    private String VIN; 
    //Reservation and Cost information
    private Cost cost; 
    private Reservation reserv; 
 
   
    //--------------------------------------------------------------------------------------
    //Constructor 
    //--------------------------------------------------------------------------------------
    public Vehicle (String descript, int mpg, String num_value, String VIN) { 
        this.descript = descript; 
        this.mpg = mpg; 
        this.num_value = num_value; 
        this.VIN = VIN; 
        reserv = null; 
        cost = null; 
    }//Close Constructor
    
    //--------------------------------------------------------------------------------------
    //abstract toString method
    //--------------------------------------------------------------------------------------
    public abstract String toString ();
    
    //--------------------------------------------------------------------------------------
    //getters for basic vehicle information
    //--------------------------------------------------------------------------------------
    public String getDescript () { 
        return descript; 
    }//Close getDescription
    
    public int getMpg () { 
        return mpg; 
    }//Close getMpg
 
    public String getNumValue () { 
        return num_value; 
    }//Close getNumValue
    
    public String getVIN () { 
        return VIN; 
    }//Close getVin
    
    //--------------------------------------------------------------------------------------
    //boolean method for checking if reserved
    //--------------------------------------------------------------------------------------
    public boolean isReserved() { 
        return reserv != null; 
    }//Close isReserved

    //--------------------------------------------------------------------------------------
    //getters and setters for Reservation and Cost 
    //--------------------------------------------------------------------------------------
    public Cost getCost () { 
        return cost; 
    }//Close getActualRate
    
    public void setCost (Cost value) { 
        cost = value; 
    }//Close setActualrate
    
    public Reservation getReserv () { 
        return reserv; 
    }
    
    public void setReserv (Reservation value) { 
        reserv = value; 
    }//Close setReserv
    
    //--------------------------------------------------------------------------------------
    //calcCost
    //--------------------------------------------------------------------------------------
    //calculate cost (for after a vehicle returned)
    //--------------------------------------------------------------------------------------
    public double calcCost(String time_unit, int num_time_units, int num_miles_driven, boolean insur_selected) {
        //calls the calcCost method of the Cost object assigned to vehicle.
        //if of type CompanyReservation, then returns amount reduced by 15%
        double total_chrg = cost.calcCost(time_unit, num_time_units, num_miles_driven, insur_selected);
    
        if(reserv instanceof CompanyReservation)
            total_chrg = total_chrg - (total_chrg* Rates.company_discount);
		
	return total_chrg;
         
    }//Close calcCost
    //--------------------------------------------------------------------------------------
}//Close Vehicle Class
