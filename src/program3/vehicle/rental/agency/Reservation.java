//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class Reservation
//------------------------------------------------------------------------------------------
//The Reservation type object assigned will be either PrivateCustReservation or CompanyReservation type. 
//The difference is that private customer reservations provide a credit card, and company reservations 
//provide a company acct number. (How each of these numbers are checked as valid is different.) 
//******************************************************************************************
//List of methods: getName, getTimeUnit, getNumTimeUnits, getNumChargedTo, InsuranceSelected
//******************************************************************************************
//==========================================================================================
public abstract class Reservation { 
    private String name; 
    private String time_unit; //time unit (day, week, month)
    private int num_time_units; // quanitifed value of days, weeks, months
    private String num_charged_to; 
    private boolean insurance_selected; 

    
    //--------------------------------------------------------------------------------------
    //Constructor
    //--------------------------------------------------------------------------------------
    public Reservation (String name, String time_unit, int num_time_units, String num_charged_to, boolean insurance_selected) { 
        this.name = name; 
        this.time_unit = time_unit; 
        this.num_time_units = num_time_units; 
        this.num_charged_to = num_charged_to; 
        this.insurance_selected = insurance_selected;    
    }//Close Constructor 
    
    //--------------------------------------------------------------------------------------
    //abstract toString method 
    //--------------------------------------------------------------------------------------
    public abstract String toString(); 

    //--------------------------------------------------------------------------------------
    //Getters
    //--------------------------------------------------------------------------------------
    public String getName() { 
        return name; 
    }//Close getName
    
    public String getTimeUnit() { 
        return time_unit; 
    }//Close getTimeUnit
    
    public int getNumTimeUnits() { 
        return num_time_units; 
    }//Close getNumTimeUnits
    
    public String getNumChargedTo() { 
        return num_charged_to; 
    }//Close getNumChargedTo
    
    public boolean InsuranceSelected() { 
        return insurance_selected; 
    }//Close getInsuranceSelected 
   //--------------------------------------------------------------------------------------
}//Close Reservation Class
     
