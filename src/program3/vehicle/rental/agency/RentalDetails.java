//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class RentalDetails
//------------------------------------------------------------------------------------------
//RentalDetails contains the information requested from the user for estimating the cost of a 
//given rental. These objects are only used for passing information between methods. 
//******************************************************************************************
//List of methods: getVIN, getMiles, getDurationType, getRentalFrequency, getInsurOption
//******************************************************************************************
//==========================================================================================
public class RentalDetails {
    
    //--------------------------------------------------------------------------------------
    //Instance Variables
    //-------------------------------------------------------------------------------------- 
    String vin; 
    int miles; 
    String duration_type; 
    int rental_frequency; 
    boolean insur_option; 
    
    //--------------------------------------------------------------------------------------
    //Constructor
    //--------------------------------------------------------------------------------------
    public RentalDetails (String vin, int miles, String duration_type, int rental_frequency, boolean insur_option) { 
        this.vin = vin; 
        this.miles = miles; 
        this.duration_type = duration_type; 
        this.rental_frequency = rental_frequency; 
        this.insur_option = insur_option; 
    }//Close cCnstructor
    
    //--------------------------------------------------------------------------------------
    //Getters
    //--------------------------------------------------------------------------------------
    public String getVIN () { 
        return vin; 
    }//Close getVIN
    
    public int getMiles() { 
        return miles;
    }//Close getMiles
    
    public String getDurationType () { 
        return duration_type; 
    }//Close getDurationType
    
    public int getRentalFrequency ()  {
        return rental_frequency; 
    }//Close getRentalFrequency
    
    public boolean getInsurOption () { 
        return insur_option; 
    }//Close getInsurOption
    //--------------------------------------------------------------------------------------        
}//Close RentalDetails 
