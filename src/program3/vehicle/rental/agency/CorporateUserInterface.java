//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
import java.util.Scanner;
//==========================================================================================
// Class CorporateUserInterface
//------------------------------------------------------------------------------------------
//CorporateUserInterface implements UserInterface. It provides the user with a selection menu.
//Before reaching the menu, the user will be prompted to login with his/her account number. 
//Selecting an option from the menu will cause respective methods to exectute. 
//The methods will return a String array, which is displayed through the method displayLines. 
//******************************************************************************************
//List of methods: start, displayMenu, execute, login, getSelection, exec_displayCarRates, 
//exec_displaySUVRates, exec_displayTruckRates, exec_displayAvailCars, exec_displayAvailSUVs,
//exec_displayAvailTrucks, exec_calcEstimatedCost, exec_makeReservation, exec_displayReservation,
//exec_cancelReservation, displayLines
//******************************************************************************************
//==========================================================================================
public class CorporateUserInterface implements UserInterface {
    Scanner input = new Scanner (System.in); 
    private String acct_num;
    private Account corporate;
            
    //No constructor needed
    
    //Starts a "command loop" that repeatedly: displays a menu of options, gets the selected 
    //command from the user, and executed the command. 
    
    //--------------------------------------------------------------------------------------
    //start
    //--------------------------------------------------------------------------------------
    public void start(){
        
        boolean terminate = false;
        boolean check = false; 
        int selection, option; 
        String[] results; 
        String vin; 
        Cost cost; 
        
        //transient objects created for returning and passing of values (not stored)
        //must create information-storing classes RentalDetails and ReservationDetails 
        
        RentalDetails rental_details; 
        ReservationDetails reserv_details; 
        
        while (!check) {
            System.out.println ("Enter account number"); 
            acct_num = input.next(); 
            if (login(acct_num)){
                check = true; 
                corporate = SystemInterface.corporateInfo(acct_num);
                System.out.println ("Welcome: ");
                System.out.println (corporate.toString());
                while (!terminate) {
                    displayMenu(); //display numbered list of command options 
                    selection = getSelection(); //read selected command from user
                    if (selection >= 7)
                        terminate = true; 
                    execute(selection);     
                }//Close while
  
            }//Close if 
            else  { 
               System.out.println ("Invalid account number");  
            }//Close else
        }//Close while
    }//Close Start
    
    //Private methods 
    
    //--------------------------------------------------------------------------------------
    //displayMenu
    //--------------------------------------------------------------------------------------
    private void displayMenu() {    
        System.out.println ("*****************************************************");
        System.out.println (); 
        System.out.println ("1 - Display rental rates"); 
        System.out.println ("2 - Display available Vehicles");  
        System.out.println ("3 - Estimated rental cost");  
        System.out.println ("4 - Make a reservation"); 
        System.out.println ("5 - Display reservation"); 
        System.out.println ("6 - Cancel reservation");
        System.out.println ("7 - Quit"); 
        System.out.println (); 
    }//Close displayMenu
    
    //--------------------------------------------------------------------------------------
    //execute
    //--------------------------------------------------------------------------------------
    //Reacts to user selection from menu
    private void execute(int sel){
        String[] lines = new String[50];
        
        switch(sel)
        {
            case 1: pickVehicle(lines); break;
            case 2: pickAvailVehicle(lines); break; 
            case 3: exec_calcEstimatedCost(lines); break;
            case 4: exec_makeReservation(lines); break;
            case 5: exec_displayReservation(lines); break;
            case 6: exec_cancelReservation(lines); break;
            default: 
                System.out.println ("Program ended"); 
                break; 
        }//Close switch
    }//Close execute
    
    //--------------------------------------------------------------------------------------
    //getSelection
    //--------------------------------------------------------------------------------------
    private int getSelection() {
        
        int value; 
        //process
        System.out.print ("Enter selection (1-7): ");
        value = input.nextInt(); 
        return value;   
    }//Close getSelection 
    
    //--------------------------------------------------------------------------------------
    //login
    //--------------------------------------------------------------------------------------
    private boolean login(String acct_num) {
        String[] acct_nums = SystemInterface.corporateLogin();
       
        for (int i = 0; i < acct_nums.length; i++) {
            if (acct_num.equals(acct_nums[i]))
                return true; 
        }//Close for
        return false; 
    }//Close login
    
    //--------------------------------------------------------------------------------------
    //pickVehicle
    //--------------------------------------------------------------------------------------
    private void pickVehicle(String[] lines) { 
        int sel; 
        System.out.print ("Display rental rates for 1 - Cars, 2 - SUVs, or 3 - Trucks: "); 
        sel = input.nextInt(); 
        
        switch (sel) { 
            case 1: exec_displayCarRates(lines); break;
            case 2: exec_displaySUVRates(lines); break;
            case 3: exec_displayTruckRates(lines); break;
            default: System.out.println ("Invalid entry"); break; 
        }//Close switch
    }//Close pickVehicle method
    
    //--------------------------------------------------------------------------------------
    //pickAvailVehicle
    //--------------------------------------------------------------------------------------
    private void pickAvailVehicle(String[] lines) { 
        int sel; 
        System.out.print ("Display available 1 - Cars, 2 - SUVs, or 3 - Trucks: "); 
        sel = input.nextInt(); 
        
        switch (sel) { 
            case 1: exec_displayAvailCars(lines); break;
            case 2: exec_displayAvailSUVs(lines); break;
            case 3: exec_displayAvailTrucks(lines); break;
            default: System.out.println ("Invalid entry"); break; 
        }//Close switch
    }//Close pickVehicle method
    //--------------------------------------------------------------------------------------
    //exec_displayCarRates
    //--------------------------------------------------------------------------------------
    private void exec_displayCarRates(String[] lines) {
         lines = SystemInterface.getCarRates(true);
         displayLines(lines);
    }//Close exec_displayCarRates
    
    //--------------------------------------------------------------------------------------
    //exec_displaySUVRates
    //--------------------------------------------------------------------------------------
    private void exec_displaySUVRates(String[] lines) {
         lines = SystemInterface.getSUVRates(true);
         displayLines(lines);
    }//Close exec_displaySUVRates
    
    //--------------------------------------------------------------------------------------
    //exec_displayTruckRates
    //--------------------------------------------------------------------------------------
    private void exec_displayTruckRates(String[] lines) { 
        lines = SystemInterface.getTruckRates(true); 
        displayLines (lines);   
    }//Close exec_displayTruckRates
    
    //--------------------------------------------------------------------------------------
    //exec_displayAvailCars
    //--------------------------------------------------------------------------------------
    private void exec_displayAvailCars(String[] lines) { 
        lines = SystemInterface.getAvailCars(); 
        displayLines (lines); 
    }//Close exec_displayAvailCars
    
    //--------------------------------------------------------------------------------------
    //exec_displayAvailSUVs
    //--------------------------------------------------------------------------------------
    private void exec_displayAvailSUVs(String[] lines) { 
        lines = SystemInterface.getAvailSUVs(); 
        displayLines (lines); 
    }//Close exec_displayAvailSUVs
    
    //--------------------------------------------------------------------------------------
    //exec_displayAvailTrucks
    //--------------------------------------------------------------------------------------
    private void exec_displayAvailTrucks(String[] lines) { 
        lines = SystemInterface.getAvailTrucks(); 
        displayLines (lines); 
    }//Close exec_displayAvailTrucks
    
    //--------------------------------------------------------------------------------------
    //exec_calcEstimatedCost
    //--------------------------------------------------------------------------------------
    //User must input information before cost is estimated
    private void exec_calcEstimatedCost(String[] lines) { 
        //declaration
        String VIN; 
        int miles; 
        String duration_type; 
        int rental_frequency; 
      
        //process
        System.out.println ("Enter rental info");
        System.out.println ("Enter VIN"); 
        System.out.print ("Enter: "); 
        VIN = input.next(); 
        System.out.println ("Enter number of miles"); 
        System.out.print ("Enter: "); 
        miles = input.nextInt(); 
        System.out.println("Enter time period ('daily', 'weekly', or 'monthly')");
        System.out.print ("Enter: "); 
        duration_type = input.next(); 
        System.out.println ("Enter duration (single number) of time period"); 
        System.out.print ("Enter: "); 
        rental_frequency = input.nextInt();  
        
        //method call
        RentalDetails details = new RentalDetails(VIN, miles, duration_type, rental_frequency, corporate.insuranceDesired()); 
        lines = SystemInterface.estimatedRentalCost(details); 
        displayLines (lines); 
    }//Close exec_calcEstimatedCost
    
    //--------------------------------------------------------------------------------------
    //exec_makeReservation
    //--------------------------------------------------------------------------------------
    //User must input information before reservation can be made
    private void exec_makeReservation(String[] lines) { 
        //declaration
        String time_period, VIN;
        int rental_duration; 
         
        //process
        System.out.println ("Enter rental info");
        System.out.println ("Enter time period (i.e. 'days', 'weeks')"); 
        System.out.print ("Enter: "); 
        time_period = input.next(); 
        System.out.println ("Enter duration (single number) of time period"); 
        System.out.print ("Enter: "); 
        rental_duration = input.nextInt(); 
        System.out.println ("Enter VIN"); 
        System.out.print ("Enter: "); 
        VIN = input.next(); 
        
        
        //method call
        //(String vin, String name, String time_unit, int num_time_units, String num_charged_to, boolean insurance_selected) { 
        ReservationDetails details = new ReservationDetails(VIN, corporate.getName(), time_period, rental_duration, acct_num, corporate.insuranceDesired()); 
        lines = SystemInterface.makeCorporateReservation(details);
        displayLines (lines);
    }//Close exec_makeReservation
    
    //--------------------------------------------------------------------------------------
    //exec_displayReservation
    //--------------------------------------------------------------------------------------
    private void exec_displayReservation(String[] lines) {
        String vin; 
        System.out.println ("Enter VIN");
        System.out.print ("Enter: "); 
        vin = input.next(); 
        
        lines = SystemInterface.getAllReservsByCompany (acct_num, vin); 
        displayLines (lines); 
    }//Close exec_displayReservation
    
    //--------------------------------------------------------------------------------------
    //exec_cancelReservation
    //--------------------------------------------------------------------------------------
    private void exec_cancelReservation(String[] lines) { 
        //declaration
        String vin; 
        
        //process
        System.out.println ("Enter VIN number"); 
        System.out.print ("Enter: "); 
        vin = input.next(); 
        
        //method call
        lines = SystemInterface.cancelReservByCreditCard (acct_num, vin); 
        displayLines (lines); 
    }//Close exec_cancalReservation
    
    //--------------------------------------------------------------------------------------
    //displayLines
    //--------------------------------------------------------------------------------------
    private void displayLines(String[] lines) {   
        
        System.out.println();
        for (int i = 0; i < lines.length; i++) {
            if (lines[i] == null)
                System.out.print("");
            else 
                System.out.println (lines[i]);
        }//Close for
    }//Close displayLines
    //--------------------------------------------------------------------------------------
}//Close Class PrivateCustomerUI


