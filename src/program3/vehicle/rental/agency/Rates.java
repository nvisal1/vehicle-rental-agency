//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class Rates
//------------------------------------------------------------------------------------------
//Rates stores the current rental rates of the rental agency, for either Cars, SUVs or Trucks. 
//They are hard-coded to construct themselves with the rates for daily, weekly, and monthly rental, 
//as well as the per-mile charge and the daily charge for insurance.
//******************************************************************************************
//List of methods: getPerDay, getPerWeek, getPerMonth, getPerMile, getDailyInsur, toString
//******************************************************************************************
//==========================================================================================
public class Rates {
    
    //--------------------------------------------------------------------------------------
    //Instance Variables 
    //--------------------------------------------------------------------------------------
    private double perDay; 
    private double perWeek; 
    private double perMonth; 
    private double perMile; 
    private double dailyInsur; 
    //15% discount to companies for all vehicle types
    public static final double company_discount = 0.15;
    
    //--------------------------------------------------------------------------------------
    //Constructor 
    //--------------------------------------------------------------------------------------
    public Rates (double perDay, double perWeek, double perMonth, double perMile, double dailyInsur) { 
        
        this.perDay = perDay; 
        this.perWeek = perWeek; 
        this.perMonth = perMonth; 
        this.perMile = perMile; 
        this.dailyInsur = dailyInsur; 
  
    }//Close constructor 
    
    //--------------------------------------------------------------------------------------
    //Getters
    //--------------------------------------------------------------------------------------
    public double getPerDay() { 
        return perDay;
    }//Close getPerDay 
    
    public double getPerWeek() { 
        return perWeek; 
    }//Close getPerWeek
    
    public double getPerMonth() { 
        return perMonth; 
    }//Close getPerMonth 
    
    public double getPerMile() { 
        return perMile; 
    }//Close getPerMile 
    
    public double getDailyInsur() {
        return dailyInsur; 
    }//Close getDailyInsur
    
    //--------------------------------------------------------------------------------------
    //toString 
    //--------------------------------------------------------------------------------------
    public String toString() {
        return "Daily      Weekly      Monthly        Per Mile        Daily Insurance\n"+"$"+perDay +"     "+"$"+perWeek+"     "+"$"+perMonth+"        "+"$"+perMile+"     "+"      $"+dailyInsur;    
    }//Close toString
    //--------------------------------------------------------------------------------------
}//Close Rates Class
