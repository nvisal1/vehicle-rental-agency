//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
import java.util.Scanner; 
//==========================================================================================
// Class ManagerUserInterface
//------------------------------------------------------------------------------------------
//ManagerUserInterface implements UserInterface. It provides a selection menu for managers. 
//Selecting an option from the menu will cause respective methods to exectute. 
//The methods will return a String array, which is displayed through the method displayLines. 
//******************************************************************************************
//List of methods: start, displayMenu, execute, getSelection, exec_displayCarRates, 
//exec_displaySUVRates, exec_displayTruckRates, exec_displayAllVehicles, exec_displayAllReservations,
//exec_addCorporateAccount, exec_displayAllCorporateAccounts, displayLines
//******************************************************************************************
//==========================================================================================
public class ManagerUserInterface implements UserInterface {
    Scanner input = new Scanner (System.in); 
    //No constructor needed
    
    //Starts a "command loop" that repeatedly: displays a menu of options, gets the selected 
    //command from the user, and executed the command. 
    
    //--------------------------------------------------------------------------------------
    //start
    //--------------------------------------------------------------------------------------
    public void start() {
        
        Scanner input = new Scanner (System.in); 
        boolean terminate = false;
        int selection, option; 
        String[] results; 
        String acct_num; 
        String vin; 
        Cost cost; 
        
        //transient objects created for returning and passing of values (not stored)
        //must create information-storing classes RentalDetails and ReservationDetails 
        
        RentalDetails rental_details; 
        ReservationDetails reserv_details; 
        
        while (!terminate) {
            displayMenu(); //display numbered list of command options 
            selection = getSelection(); //read selected command from user
            if (selection >= 6)
                   terminate = true; 
            execute(selection);      
        }//Close while 
   
    }//Close Start
    
    //Private methods 
    
    //--------------------------------------------------------------------------------------
    //displayMenu
    //--------------------------------------------------------------------------------------
    private void displayMenu() {      
        System.out.println ("*****************************************************");
        System.out.println ();
        System.out.println ("1 - Display rental rates");
        System.out.println ("2 - Display all vehicles");
        System.out.println ("3 - Display all reservations"); 
        System.out.println ("4 - Add a new corporate account");
        System.out.println ("5 - Displays all accounts"); 
        System.out.println ("6 - Quit"); 
        System.out.println ();
    }//Close displayMenu
    
    //--------------------------------------------------------------------------------------
    //Instance execute
    //--------------------------------------------------------------------------------------
    //reacts to user selection from menu
    private void execute(int sel){
        String[] lines = new String[50];
        
        switch(sel)
        {
            case 1: pickVehicle(lines); break;
            case 2: exec_displayAllVehicles(lines); break;
            case 3: exec_displayAllReservations(lines); break;
            case 4: exec_addCorporateAccount(lines); break;
            case 5: exec_displayAllCorporateAccounts(lines); break;
            default: 
                System.out.println ("Program ended"); 
                break; 
        }//Close switch
    }//Close execute
    
    //--------------------------------------------------------------------------------------
    //getSelection
    //--------------------------------------------------------------------------------------
    private int getSelection() {
        //declaration
        int value;
        
        //process
        System.out.print ("Enter selection (1-6): ");
        value = input.nextInt(); 
        
        //return
        return value;   
    }//Close getSelection 
    
    //--------------------------------------------------------------------------------------
    //pickVehicle
    //--------------------------------------------------------------------------------------
    private void pickVehicle(String[] lines) { 
        int sel; 
        System.out.print ("Display rental rates for 1 - Cars, 2 - SUVs, or 3 - Trucks: "); 
        sel = input.nextInt(); 
        
        switch (sel) { 
            case 1: exec_displayCarRates(lines); break;
            case 2: exec_displaySUVRates(lines); break;
            case 3: exec_displayTruckRates(lines); break;
            default: System.out.println ("Invalid entry"); break; 
        }//Close switch
    }//Close pickVehicle method
                
    //--------------------------------------------------------------------------------------
    //Instance exec_displayCarRates
    //--------------------------------------------------------------------------------------
    private void exec_displayCarRates(String[] lines) {
         lines = SystemInterface.getCarRates(false);
         displayLines(lines);
    }//Close exec_displayCarRates

    //--------------------------------------------------------------------------------------
    //exec_displaySUVRates
    //--------------------------------------------------------------------------------------
    private void exec_displaySUVRates(String[] lines) {
         lines = SystemInterface.getSUVRates(false);
         displayLines(lines);
    }//Close exec_displaySUVRates
    
    //--------------------------------------------------------------------------------------
    //exec_displayTruckRates
    //--------------------------------------------------------------------------------------
    private void exec_displayTruckRates(String[] lines) { 
        lines = SystemInterface.getTruckRates(false); 
        displayLines (lines);   
    }//Close exec_displayTruckRates
    
    //--------------------------------------------------------------------------------------
    //exec_displayAllVehicles
    //--------------------------------------------------------------------------------------
    private void exec_displayAllVehicles(String[] lines) { 
        lines  = SystemInterface.getAllVehicles(); 
        displayLines (lines); 
    }//Close exec_displayAllVehicles
    
    //--------------------------------------------------------------------------------------
    //exec_displayAllReservations
    //--------------------------------------------------------------------------------------
    private void exec_displayAllReservations(String[] lines) { 
        lines = SystemInterface.getAllReserv(); 
        displayLines (lines); 
    }//Close exec_displayAllReservations
    
    //--------------------------------------------------------------------------------------
    //exec_addCorporateAccount
    //--------------------------------------------------------------------------------------
    private void exec_addCorporateAccount(String[] lines) { 
        //declaration
        String name, acct_num; 
        boolean insur; 
        
        //process
        System.out.println ("Enter Account Name"); 
        System.out.print ("Enter: "); 
        name = input.next(); 
        
        System.out.println ("Will this account recieve insurance (enter 'true' or 'false')"); 
        System.out.print ("Enter: "); 
        insur = input.nextBoolean(); 
        
        //method call
        Account acct = new Account (name, null, insur); 
        lines = SystemInterface.addAccount(acct);
        displayLines (lines); 
    }//Close exec_addCorporateAccount
    
    //--------------------------------------------------------------------------------------
    //exec_displayAllCorporateAccounts
    //--------------------------------------------------------------------------------------
    private void exec_displayAllCorporateAccounts(String[] lines) { 
        lines = SystemInterface.getAllAccounts();
        displayLines (lines); 
    }//Close exec_displayAllCorporateAccounts
 
    //--------------------------------------------------------------------------------------
    //displayLines
    //--------------------------------------------------------------------------------------
    private void displayLines(String[] lines) {   
        
        System.out.println();
        for (int i = 0; i < lines.length; i++) {
            if (lines[i] == null)
                System.out.print("");
            else 
                System.out.println (lines[i]); 
        }//Close for
    }//Close displayLines
    //--------------------------------------------------------------------------------------
}//Close Class PrivateCustomerUI


