//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Interface VehiclesIterator
//------------------------------------------------------------------------------------------
//VehiclesIterator uses methods hasNext and next to allow the program to traverse through 
//existing vehicles.
//******************************************************************************************
//List of methods: hasNext, next
//******************************************************************************************
//==========================================================================================
public interface VehiclesIterator {
    //--------------------------------------------------------------------------------------
    //Interface Methods
    //--------------------------------------------------------------------------------------
    public boolean hasNext(); 
    public Vehicle next();
    //--------------------------------------------------------------------------------------
}//Close VehiclesIterator Interface
