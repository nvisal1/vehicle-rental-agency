//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class ReservationDetails
//------------------------------------------------------------------------------------------
//ReservationDetails contains the information requested from the user for reservations.
//These objects are only used for passing information between methods. 
//******************************************************************************************
//List of methods: getVIN, getName, getTimeUnit, getNumTimeUnits, getNumChargedTo,
//InsuranceSelected
//******************************************************************************************
//==========================================================================================
public class ReservationDetails {
    //--------------------------------------------------------------------------------------
    //Instance Variables
    //--------------------------------------------------------------------------------------
    private String vin; 
    private String name; 
    private String time_unit; //time unit (day, week, month)
    private int num_time_units; // quanitifed value of days, weeks, months
    private String num_charged_to; 
    private boolean insurance_selected; 

    //--------------------------------------------------------------------------------------
    //Constructor
    //--------------------------------------------------------------------------------------
    public ReservationDetails (String vin, String name, String time_unit, int num_time_units, String num_charged_to, boolean insurance_selected) { 
        this.vin = vin; 
        this.name = name; 
        this.time_unit = time_unit; 
        this.num_time_units = num_time_units; 
        this.num_charged_to = num_charged_to; 
        this.insurance_selected = insurance_selected;  
    }//Close Constructor
   
    //--------------------------------------------------------------------------------------
    //Getters
    //--------------------------------------------------------------------------------------
    public String getVIN() { 
        return vin; 
    }//Close getVIN
    
    public String getName() { 
        return name; 
    }//Close getName
    
    public String getTimeUnit() { 
        return time_unit; 
    }//Close getTimeUnit
    
    public int getNumTimeUnits() { 
        return num_time_units; 
    }//Close getNumTimeUnits
    
    public String getNumChargedTo() { 
        return num_charged_to; 
    }//Close getNumChargedTo
    
    public boolean InsuranceSelected() { 
        return insurance_selected; 
    }//Close getInsuranceSelected 
    //--------------------------------------------------------------------------------------
}//Close ReservationDetails Class
