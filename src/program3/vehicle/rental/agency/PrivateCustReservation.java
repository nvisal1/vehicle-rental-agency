//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Subclass PrivateCustReservation
//------------------------------------------------------------------------------------------
//PrivateCustReservation stores reservation information for private customers
//******************************************************************************************
//List of methods: toString 
//******************************************************************************************
//==========================================================================================

public class PrivateCustReservation extends Reservation {
    
    //--------------------------------------------------------------------------------------
    //Constructor 
    //--------------------------------------------------------------------------------------
    //Uses parent (Reservation) constructor
    //Throws exception if the string credit_card is not a valid credit card number (i.e., 16 digits)
    //-------------------------------------------------------------------------------------- 
    public PrivateCustReservation (String name, String time_unit, int num_time_units, String creditcard_num, boolean insurance_selected) {
            super (name, time_unit, num_time_units, Utilities.validateCreditCard(creditcard_num), insurance_selected);
	}//Close Constructor 
     
    //-------------------------------------------------------------------------------------- 
    //toString 
    //-------------------------------------------------------------------------------------- 
    public String toString () {
      if (InsuranceSelected() == true)
            return "Name: " + getName() + " " + " Time: " + getNumTimeUnits() + " " + getTimeUnit() + " " + " Card Number :" + getNumChargedTo() + " " + " Insurance: Yes"; 
        else { 
            return "Name: " + getName() + " " + " Time: " + getNumTimeUnits() + " " + getTimeUnit() + " " + " Card Number :" + getNumChargedTo() + " " + " Insurance: No"; 
        }
    }//Close toString 
    //-------------------------------------------------------------------------------------- 
}//Close PrivateCustReservation Class
