//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Subclass CarCost
//------------------------------------------------------------------------------------------
//CarCost stores cost information for a specfic vehicle type 
//******************************************************************************************
//List of methods: toString
//******************************************************************************************
//==========================================================================================
public class CarCost extends Cost {
    
    //--------------------------------------------------------------------------------------
    //Constructor 
    //--------------------------------------------------------------------------------------
    //Uses parent (Cost) constructor
    //--------------------------------------------------------------------------------------
    public CarCost(double cost_per_day, double cost_per_week, double cost_per_month, double per_mile_charge, double daily_insur_cost) {
        super (cost_per_day, cost_per_week, cost_per_month, per_mile_charge, daily_insur_cost);
    }//Close constructor 
    
    //--------------------------------------------------------------------------------------
    //toString 
    //--------------------------------------------------------------------------------------
    public String toString () { 
        return "Daily: " + getCostPerDay() + " " + "Weekly: " + getCostPerWeek() + " " + "Monthly: " + getCostPerMonth() + " " + "Miles: " + getPerMileCharge() + " " + "Insurance: " + getDailyInsurCost(); 
    }//Close toString 
    //--------------------------------------------------------------------------------------
}//Close CarCost Class
