//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Subclass CompanyReservation
//------------------------------------------------------------------------------------------
//CompanyReservation stores reservation information for companies
//******************************************************************************************
//List of methods: toString
//******************************************************************************************
//==========================================================================================
public class CompanyReservation extends Reservation { 
    //--------------------------------------------------------------------------------------
    //Constructor 
    //--------------------------------------------------------------------------------------
    //Uses parent (Reservation) constructor
    //-------------------------------------------------------------------------------------- 
    public CompanyReservation (String name, String time_unit, int num_time_units, String acct_num, boolean insurance_selected) {
        
        super(name, time_unit, num_time_units, acct_num, insurance_selected);

    }//Close Constructor 

    //-------------------------------------------------------------------------------------- 
    //toString 
    //-------------------------------------------------------------------------------------- 
    public String toString () {
        if (InsuranceSelected() == true)
            return "Name: " + getName() + " " + " Time: " + getNumTimeUnits() + " " + getTimeUnit() + " " + " Account Number :" + getNumChargedTo() + " " + " Insurance: Yes"; 
        else { 
            return "Name: " + getName() + " " + " Time: " + getNumTimeUnits() + " " + getTimeUnit() + " " + " Account Number :" + getNumChargedTo() + " " + " Insurance: No"; 
        }
    }//Close toString
    //-------------------------------------------------------------------------------------- 
}//Close CompanyReservation Class


    

