//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
import java.util.Scanner; 
//==========================================================================================
// Class PrivateCustomerUI 
//------------------------------------------------------------------------------------------
//PrivateCustomerUI implements UserInterface. It provides a selection menu for regualar customers. 
//Selecting an option from the menu will cause respective methods to exectute. 
//The methods will return a String array, which is displayed through the method displayLines. 
//******************************************************************************************
//List of methods: start, displayMenu, execute, getSelection, exec_displayCarRates, 
//exec_displaySUVRates, exec_displayTruckRates, exec_displayAvailCars, exec_displayAvailSUVs,
//exec_displayAvailTrucks, exec_calcEstimatedCost, exec_makeReservation, exec_displayReservation
//exec_cancelReservation, displayLines
//******************************************************************************************
//==========================================================================================

public class PrivateCustomerUI implements UserInterface {
    Scanner input = new Scanner (System.in); 
    //No constructor needed
    
    //Starts a "command loop" that repeatedly: displays a menu of options, gets the selected 
    //command from the user, and executed the command. 
   
    //--------------------------------------------------------------------------------------
    //start
    //--------------------------------------------------------------------------------------
    public void start(){
        
        Scanner input = new Scanner (System.in); 
        boolean terminate = false;
        int selection, option; 
        String[] results; 
        String credit_card_num; 
        String vin; 
        Cost cost; 
        
        //transient objects created for returning and passing of values (not stored)
        //must create information-storing classes RentalDetails and ReservationDetails 
        
        RentalDetails rental_details; 
        ReservationDetails reserv_details; 
        
        while (!terminate) {
            displayMenu(); //display numbered list of command options 
            selection = getSelection(); //read selected command from user
            if (selection >= 7)
                terminate = true;
            execute(selection);      
        }//Close while 
   
    }//Close Start
    
    //Private methods 

    //--------------------------------------------------------------------------------------
    //displayMenu
    //--------------------------------------------------------------------------------------
    private void displayMenu() {      
        System.out.println ("*****************************************************");
        System.out.println ();  
        System.out.println ("1 - Display rental rates"); 
        System.out.println ("2 - Display available Vehicles");
        System.out.println ("3 - Estimated rental cost");  
        System.out.println ("4 - Make a reservation"); 
        System.out.println ("5 - Display reservation"); 
        System.out.println ("6 - Cancel reservation");
        System.out.println ("7 - Quit"); 
        System.out.println (); 
    }//Close displayMenu
    
    //--------------------------------------------------------------------------------------
    //execute
    //--------------------------------------------------------------------------------------
    private void execute(int sel){
        String[] lines = new String[50];
        
        switch(sel)
        {
            case 1: pickVehicle(lines); break;
            case 2: pickAvailVehicle(lines); break;
            case 3: exec_calcEstimatedCost(lines); break;
            case 4: exec_makeReservation(lines); break;
            case 5: exec_displayReservation(lines); break;
            case 6:exec_cancelReservation(lines); break;
            default: 
                System.out.println ("Program ended"); 
                break; 
        }//Close switch
    }//Close execute
    
    //--------------------------------------------------------------------------------------
    //getSelection
    //--------------------------------------------------------------------------------------
    private int getSelection() {
        //declaration
        int value; 
        //process
        System.out.print ("Enter selection (1-7): ");
        value = input.nextInt(); 
        //return
        return value;   
    }//Close getSelection 
    
    //--------------------------------------------------------------------------------------
    //pickVehicle
    //--------------------------------------------------------------------------------------
    private void pickVehicle(String[] lines) { 
        int sel; 
        System.out.print ("Display rental rates for 1 - Cars, 2 - SUVs, or 3 - Trucks: "); 
        sel = input.nextInt(); 
        
        switch (sel) { 
            case 1: exec_displayCarRates(lines); break;
            case 2: exec_displaySUVRates(lines); break;
            case 3: exec_displayTruckRates(lines); break;
            default: System.out.println ("Invalid entry"); break; 
        }//Close switch
    }//Close pickVehicle method
    
    //--------------------------------------------------------------------------------------
    //pickVehicle
    //--------------------------------------------------------------------------------------
    private void pickAvailVehicle(String[] lines) { 
        int sel; 
        System.out.print ("Display available 1 - Cars, 2 - SUVs, or 3 - Trucks: "); 
        sel = input.nextInt(); 
        
        switch (sel) { 
            case 1: exec_displayAvailCars(lines); break;
            case 2: exec_displayAvailSUVs(lines); break;
            case 3: exec_displayAvailTrucks(lines); break;
            default: System.out.println ("Invalid entry"); break; 
        }//Close switch
    }//Close pickVehicle method
    
    //--------------------------------------------------------------------------------------
    //exec_displayCarRates
    //--------------------------------------------------------------------------------------
    private void exec_displayCarRates(String[] lines) {
         lines = SystemInterface.getCarRates(false);
         displayLines(lines);
    }//Close exec_displayCarRates

    //--------------------------------------------------------------------------------------
    //exec_displaySUVRates
    //--------------------------------------------------------------------------------------
    private void exec_displaySUVRates(String[] lines) {
         lines = SystemInterface.getSUVRates(false);
         displayLines(lines);
    }//Close exec_displaySUVRates
    
    //--------------------------------------------------------------------------------------
    //exec_displayTruckRates
    //--------------------------------------------------------------------------------------
    private void exec_displayTruckRates(String[] lines) {  
        lines = SystemInterface.getTruckRates(false); 
        displayLines (lines);   
    }//Close exec_displayTruckRates
    
    //--------------------------------------------------------------------------------------
    //exec_displayAvailCars
    //--------------------------------------------------------------------------------------
    private void exec_displayAvailCars(String[] lines) { 
        lines = SystemInterface.getAvailCars(); 
        displayLines (lines); 
    }//Close exec_displayAvailCars
    
    //--------------------------------------------------------------------------------------
    //exec_displayAvailSUVs
    //--------------------------------------------------------------------------------------
    private void exec_displayAvailSUVs(String[] lines) { 
        lines = SystemInterface.getAvailSUVs(); 
        displayLines (lines); 
    }//Close exec_displayAvailSUVs
    
    //--------------------------------------------------------------------------------------
    //exec_displayAvailTrucks
    //--------------------------------------------------------------------------------------
    private void exec_displayAvailTrucks(String[] lines) { 
        lines = SystemInterface.getAvailTrucks(); 
        displayLines (lines); 
    }//Close exec_displayAvailTrucks
    
    //--------------------------------------------------------------------------------------
    //exec_calcEstimatedCost
    //--------------------------------------------------------------------------------------
    //User must input information beofre cost can be estimated
    private void exec_calcEstimatedCost(String[] lines) { 
        //declarations
        String VIN; 
        int miles; 
        String duration_type; 
        int rental_frequency; 
        boolean insur_option;
      
        //process
        System.out.println ("Enter rental info");
        System.out.println ("Enter VIN"); 
        System.out.print ("Enter: "); 
        VIN = input.next(); 
        System.out.println ("Enter number of miles");
        System.out.print ("Enter: "); 
        miles = input.nextInt(); 
        System.out.println("Enter time period ('daily', 'weekly', or 'monthly')");
        System.out.print ("Enter: "); 
        duration_type = input.next(); 
        System.out.println ("Enter duration (single number) of time period"); 
        System.out.print ("Enter: "); 
        rental_frequency = input.nextInt(); 
        System.out.println ("Enter 'true' to get insurance. Enter 'false' otherwise."); 
        System.out.print ("Enter: "); 
        insur_option = input.nextBoolean(); 
        
        //method call
        RentalDetails details = new RentalDetails(VIN, miles, duration_type, rental_frequency, insur_option); 
        lines = SystemInterface.estimatedRentalCost(details); 
        displayLines (lines); 
    }//Close exec_calcEstimatedCost
    
    //--------------------------------------------------------------------------------------
    //exec_makeReservation
    //--------------------------------------------------------------------------------------
    //User must input information before reservation can be made
    private void exec_makeReservation(String[] lines) { 
       
        //declaration
        String name, time_period, num_charged_to, VIN;
        int rental_duration; 
        boolean insur_selected; 
        
        //process
        System.out.println ("Enter rental info");
        System.out.println ("Enter name"); 
        System.out.print ("Enter: "); 
        name = input.next(); 
        System.out.println ("Enter time period (i.e. 'days', 'weeks')"); 
        System.out.print ("Enter: "); 
        time_period = input.next(); 
        System.out.println ("Enter duration (single number) of time period"); 
        System.out.print ("Enter: "); 
        rental_duration = input.nextInt(); 
        System.out.println ("Enter VIN"); 
        System.out.print ("Enter: "); 
        VIN = input.next(); 
        System.out.println ("Enter number to be charged"); 
        System.out.print ("Enter: "); 
        num_charged_to = input.next(); 
        System.out.println ("Enter 'true' to get insurance. Enter 'false' otherwise."); 
        System.out.print ("Enter: "); 
        insur_selected = input.nextBoolean(); 
        
        //method call
        ReservationDetails details = new ReservationDetails(VIN, name, time_period, rental_duration, num_charged_to, insur_selected);
        lines = SystemInterface.makePrivateReservation(details);
        displayLines (lines);
    }//Close exec_makeReservation
    
    //--------------------------------------------------------------------------------------
    //exec_displayReservation
    //--------------------------------------------------------------------------------------
    //User must input information beofre reservation can be displayed
    private void exec_displayReservation(String[] lines) { 
        //declaration
        String card_num, vin; 
        
        //process
        System.out.println ("Enter card number");
        System.out.print ("Enter: "); 
        card_num = input.next(); 
        
        System.out.println ("Enter VIN"); 
        System.out.print ("Enter: "); 
        vin = input.next(); 
        
        //method call
        lines = SystemInterface.getReservByCreditCard (card_num, vin); 
        displayLines (lines); 
    }//Close exec_displayReservation
    
    //--------------------------------------------------------------------------------------
    //exec_cancelReservation
    //--------------------------------------------------------------------------------------
    //User must input information before reservation can be terminated
    private void exec_cancelReservation(String[] lines) { 
        //declaration
        String card_num, vin; 
        
        //process
        System.out.println("Enter card number");
        System.out.print ("Enter: "); 
        card_num = input.next(); 
        System.out.println ("Enter VIN number"); 
        System.out.print ("Enter: "); 
        vin = input.next(); 
        
        //method call
        lines = SystemInterface.cancelReservByCreditCard (card_num, vin); 
        displayLines (lines); 
    }//Close exec_cancalReservation
    
    //--------------------------------------------------------------------------------------
    //displayLines
    //--------------------------------------------------------------------------------------
    private void displayLines(String[] lines) {   
        
        System.out.println();
        for (int i = 0; i < lines.length; i++) {
            if (lines[i] == null)
                System.out.print("");
            else 
                System.out.println (lines[i]); 
        }//Close for 
    }//Close displayLines
    //--------------------------------------------------------------------------------------
}//Close Class PrivateCustomerUI

