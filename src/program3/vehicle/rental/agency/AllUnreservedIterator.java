//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class AllUnreservedIterator
//------------------------------------------------------------------------------------------
//AllUnreservedIterator implements VehiclesIterator. This class uses hasNext and next to 
//traverse all unreserved vehicles. 
//******************************************************************************************
//List of methods: hasNext, next
//******************************************************************************************
//==========================================================================================
public class AllUnreservedIterator implements VehiclesIterator  {
    //--------------------------------------------------------------------------------------
    //Instance Variables
    //--------------------------------------------------------------------------------------
    private Vehicle[] veh_list; 
    private int current; 
    
    //--------------------------------------------------------------------------------------
    //Constructor
    //--------------------------------------------------------------------------------------
    public AllUnreservedIterator(Vehicle[] list) { 
        veh_list = list; 
        current = -1; 
    }//Close constructor 
    
    //--------------------------------------------------------------------------------------
    //Methods
    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------
    //hasNext
    //--------------------------------------------------------------------------------------
    public boolean hasNext() {
        if (veh_list[current + 1] != null) {
            if (veh_list[current + 1].getReserv() == null) 
                 return true; 
        }
        else {
            for (current = current; current < veh_list.length - 1; current++) { 
                if (veh_list[current + 1] != null) {
                    if(veh_list[current + 1].getReserv() == null)
                        return true; 
                }//Close if
            }//Close for
        }//Close else
        return false; 
    }//Close hasNext
    
    //--------------------------------------------------------------------------------------
    //next
    //--------------------------------------------------------------------------------------
    public Vehicle next() { 
        return veh_list[++current]; 
    }//Close Next
    //--------------------------------------------------------------------------------------
}//Close AllUnreservedIterator Class
