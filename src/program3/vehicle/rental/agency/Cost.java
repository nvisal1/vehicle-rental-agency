//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class Cost
//------------------------------------------------------------------------------------------
//Cost contains the rates, for the particular type vehicle “attached to” (Car, SUV or Truck) 
//when reserved, constructed with the current rates in the corresponding Rates class.
//******************************************************************************************
//List of methods: getCostPerDay, getCostPerWeek, getCostPerMonth, getPerMileCharge, 
//getDailyInsurCost, toString, calcCost
//******************************************************************************************
//==========================================================================================
public abstract class Cost {
    //-------------------------------------------------------------------------------------- 
    //Instance Variables
    //-------------------------------------------------------------------------------------- 
    private double cost_per_day;
    private double cost_per_week;
    private double cost_per_month;

    private double per_mile_charge;
    private double daily_insur_cost;
    
    //--------------------------------------------------------------------------------------
    //Constructor
    //--------------------------------------------------------------------------------------
    public Cost(double cost_per_day, double cost_per_week, double cost_per_month, double per_mile_charge, double daily_insur_cost) {
        this.cost_per_day = cost_per_day; 
        this.cost_per_week = cost_per_week; 
        this.cost_per_month = cost_per_month; 
        this.per_mile_charge = per_mile_charge; 
        this.daily_insur_cost= daily_insur_cost; 
    }//Close Constructor 
 
    //--------------------------------------------------------------------------------------
    //Getters
    //--------------------------------------------------------------------------------------
    public double getCostPerDay () { 
        return cost_per_day; 
    }//Close getCostPerDay 
    
    public double getCostPerWeek () { 
        return cost_per_week; 
    }//Close getCostPerWeek
    
    public double getCostPerMonth () {
        return cost_per_month; 
    }//Close getCostPerMonth
    
    public double getPerMileCharge () { 
        return per_mile_charge; 
    }//Close getPerMileCharge
    
    public double getDailyInsurCost () { 
        return daily_insur_cost; 
    }//Close getDailyInsurCost
    
    //--------------------------------------------------------------------------------------
    //toString 
    //--------------------------------------------------------------------------------------
    public abstract String toString ();
    
    //--------------------------------------------------------------------------------------
    //calcCost
    //--------------------------------------------------------------------------------------
    //Calculates and returns the total cost of the rental
    //--------------------------------------------------------------------------------------
    public double calcCost(String time_unit, int num_time_units, int num_miles_driven, boolean insur_selected) {
	
	double unit_rate;

	switch(time_unit) {
		case "daily": 
                    unit_rate = cost_per_day; 
                    break;
		case "weekly":
                    unit_rate = cost_per_week; 
                    break;			      
                case "monthly": 
                    unit_rate = cost_per_month; 
                    break;	
                default : 
                    unit_rate = 0; 
                    break; 
        }//Close switch

	// get the total insurance cost (if selected by customer)
	double  insur_chrg = 0;

	if(insur_selected) {
            switch(time_unit) {
                case "daily": 
                    insur_chrg = num_time_units * daily_insur_cost; 
                    break;
                case "weekly": 
                    insur_chrg = num_time_units * daily_insur_cost * 7; 
                    break;
                case "monthly": 
                    insur_chrg = num_time_units * daily_insur_cost * 30; 
                    break;
                default :
                    insur_chrg = 0; 
                    break; 
            }//Close switch
        }//Close if 
                 
        // determine total vehicle use charge
        double vehicle_use_chrg = num_time_units * unit_rate;

        // determine total mileage charge
        double mileage_chrg = num_miles_driven * per_mile_charge;

        // compute the total charge (before any discounts)
	double total_rental_charge = vehicle_use_chrg + mileage_chrg + insur_chrg;
       
        return total_rental_charge; 
    }//Close calcCost
    //--------------------------------------------------------------------------------------
}//Close Cost
