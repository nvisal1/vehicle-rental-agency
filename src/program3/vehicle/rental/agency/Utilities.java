//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class Utilities
//------------------------------------------------------------------------------------------
//Utiltiies contains methods that validate pssed numbers from PrivateCustReservation and 
//CompanyReservation 
//******************************************************************************************
//List of methods: validateCreditCard, validateAcctNum
//******************************************************************************************
//==========================================================================================
public class Utilities {
    //Instance variables
    private static CorporateAccounts accounts;
    
    public static void initUtilities (CorporateAccounts accts) { 
        accounts = accts;
    }//Close initUtilities
    
    //--------------------------------------------------------------------------------------
    //validateCreditCard
    //--------------------------------------------------------------------------------------
    //Returns passed value if length = 16 / Otherwise exception is thrown 
    //--------------------------------------------------------------------------------------
    public static String validateCreditCard (String num) throws CreditException { 
        if (num.length() != 16) 
            throw new CreditException(); 
        else 
            return num; 

    }//Close validateCreditCard 
    
    //--------------------------------------------------------------------------------------
    //validateAcctNum
    //--------------------------------------------------------------------------------------
    //Returns passed value if num is not stored in Accounts array / Otherwise exception is thrown 
    //--------------------------------------------------------------------------------------
    public static String validateAcctNum (String num) throws CreditException {
        AccountsIterator itr = accounts.getAllAccountsItr(); 
            
        while (itr.hasNext()) {
            if (itr.next().getAcctNum().equals(num))
                throw new CreditException(); 
            itr.next();
        }//Close while
        
        return num; 
    }//Close validateAcctNum

    //---------------------------------------------------------------------------------------
}//Close Utilities Class
