//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class Account
//------------------------------------------------------------------------------------------
//Account stores the company name, 5-digit account number, and whether or not 
//all rentals for the company should include the daily insurance.
//******************************************************************************************
//List of methods: getName, getAcctNum, insuranceDesired, toString 
//******************************************************************************************
//==========================================================================================
public class Account {
    //--------------------------------------------------------------------------------------
    //Instance Variables
    //--------------------------------------------------------------------------------------
    private String name;	//Company name
    private String acct_num;    //5-digit account number
    private boolean insurance_option;  //default insurance option for all rentals by this company 

    //--------------------------------------------------------------------------------------
    //Constructor
    //--------------------------------------------------------------------------------------
    public Account(String name, String acct_num, boolean insur_option) {	 
        this.name = name; 
        this.acct_num = Utilities.validateAcctNum(acct_num); 
        this.insurance_option = insur_option; 
    }//Close Constructor 

    //--------------------------------------------------------------------------------------
    //Getters
    //--------------------------------------------------------------------------------------
    public String getName() {  
        return name;
    }//Close getName
    
    public String getAcctNum() { 
        return acct_num;
    }//Close getAcctNum
    
    public boolean insuranceDesired() {
        return insurance_option; 
    }//Close insuranceDesired
    
    public String toString () { 
        
        if (insuranceDesired() == true) { 
            return "Name: " + getName() + " Account Number: " + getAcctNum() + " Insurance: Yes"; 
        }//Close if 
        else { 
            return "Name: " + getName() + " Account Number: " + getAcctNum() + " Insurance: No"; 
        }//Close else
    }//Close toString
    //--------------------------------------------------------------------------------------
}//Close Account Class


