//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class Vehicles
//------------------------------------------------------------------------------------------
//Vehicles stores the complete set of Vehicle objects.
//******************************************************************************************
//List of methods: add, genVehicle, getAllVehiclesItr, getAllUnreservedVehiclesItr, 
//getAvailCarsItr, getAvailSUVsItr, getAvailTrucksItr, getReservedVehiclesItr 
//******************************************************************************************
//==========================================================================================

public class Vehicles {
    //--------------------------------------------------------------------------------------
    //Instance Variables
    //--------------------------------------------------------------------------------------
    private Vehicle[] vehicles;  
    
    //--------------------------------------------------------------------------------------
    //Constructor
    //--------------------------------------------------------------------------------------
    public Vehicles () { 
        // allocate for max of 50 Vehicles
	vehicles = new Vehicle[50];

	// init as empty set of Vehicles
	for(int i = 0; i < 50; i++)
           vehicles[i] = null; 
    }//Close Constructor 
    
    //--------------------------------------------------------------------------------------
    //add
    //--------------------------------------------------------------------------------------
    //adds a new vehicle to vehicles
    public void add (Vehicle veh) { 
        int i = nextFree();
        vehicles [i] = veh; 
    }//Close add
   
    //--------------------------------------------------------------------------------------
    //Getters
    //--------------------------------------------------------------------------------------
    public Vehicle getVehicle (String VIN) throws VINNotFoundException { 
        VehiclesIterator itr = new AllVehiclesIterator(vehicles);
      
        Vehicle[]array = new Vehicle[50]; 
        int i = 0; 
      
        while (itr.hasNext()) { 
            if (i < 50) { 
                array[0] = itr.next(); 
                
                if (array[i].getVIN().equals(VIN))
                    return array[i]; 
            }
        }//Close while
        /*
        while (itr.hasNext()) { 
           if (itr.next().getVIN().equals(VIN))
               return itr.next(); 
           itr.next();
        }//Close while
*/
        throw new VINNotFoundException (); 
    }//Close getVehicle
    
    public VehiclesIterator getAllVehiclesItr () { 
        return new AllVehiclesIterator (vehicles);
    }//Close getAllVehiclesItr
    
    public VehiclesIterator getAllUnreservedVehiclesItr () { 
        return new AllUnreservedIterator (vehicles); 
    }//Close getAllUnreservedVehiclesItr
    
    public VehiclesIterator getAvailCarsItr() { 
        return new AvailCarsIterator (vehicles); 
    }//Close getAvailCarsItr
    
    public VehiclesIterator getAvailSUVsItr() { 
        return new AvailSUVIterator (vehicles); 
    }//Close getAvailSUVsItr
    
    public VehiclesIterator getAvailTrucksItr() { 
        return new AvailTrucksIterator (vehicles); 
    }//Close getAvailTrucksItr
    
    public VehiclesIterator getAllPrivReservedItr() { 
        return new AllPrivReservedIterator (vehicles); 
    }//Close AllReservedIterator 
    
    public VehiclesIterator getReservedVehiclesItr() { 
        return new AllReservedIterator (vehicles); 
    }//Close getreservedVehiclesItr

    //Private methods 
    //--------------------------------------------------------------------------------------
    //nextFree
    //--------------------------------------------------------------------------------------
    //determines the next available location in vehicles array 
    private int nextFree () {
        for (int i = 0; i < 50; i++) {
              if (vehicles[i] == null) 
                  return i;
        }//Close for
        return -1; 
    }//Close nextFree
    //--------------------------------------------------------------------------------------
}//Close Vehicles Class
