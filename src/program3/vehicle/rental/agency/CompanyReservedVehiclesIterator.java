//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Class CompanyReservedVehiclesIterator
//------------------------------------------------------------------------------------------
//CompanyReservedVehiclesIterator implements VehiclesIterator. This class uses hasNext and next to traverse
//through all company-reserved vehicles. 
//******************************************************************************************
//List of methods: hasNext, next
//******************************************************************************************
//==========================================================================================
public class CompanyReservedVehiclesIterator implements VehiclesIterator{
    //--------------------------------------------------------------------------------------
    //Instance Variables
    //--------------------------------------------------------------------------------------
    private Vehicle[] veh_list; 
    private int current; 
    
    //--------------------------------------------------------------------------------------
    //Constructor
    //-------------------------------------------------------------------------------------- 
    public CompanyReservedVehiclesIterator (Vehicle[] veh_list) { 
        this.veh_list = veh_list; 
        current = 0; 
    }//Close constructor 
    
    //--------------------------------------------------------------------------------------
    //Methods
    //--------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------
    //hasNext
    //--------------------------------------------------------------------------------------
    public boolean hasNext() {
        if (veh_list[current + 1].getReserv() instanceof CompanyReservation)
            return true; 
        else {
            for (current = current; current < veh_list.length - 1; current++) { 
                if (veh_list[current + 1].getReserv() instanceof CompanyReservation)
                    return true; 
            }//Close for
        }//Close else
        return false; 
    }//Close hasNext
    
    //--------------------------------------------------------------------------------------
    //next
    //--------------------------------------------------------------------------------------
    public Vehicle next () { 
        return veh_list[++current]; 
    }//Close next
    //--------------------------------------------------------------------------------------
}//Close CompanyReservedVehiclesIterator Class
