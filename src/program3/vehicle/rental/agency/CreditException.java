//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//------------------------------------------------------------------------------------------
//Exception that is used in Utilities Class
//------------------------------------------------------------------------------------------
public class CreditException extends RuntimeException {
    //Nothing to implement 
}//Close CreditException 
