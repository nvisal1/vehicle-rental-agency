//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
import java.util.Scanner;

//==========================================================================================
// Class SystemInterface
//------------------------------------------------------------------------------------------
//SystemInterface contains the methods that the user interface call to execute. 
//These methods return String arrays.
//******************************************************************************************
//List of methods: initSystem, getCarRates, getSUVRates, getTruckRates, getAvailCars, getAvailSUVs, 
//getAvailTrucks, getAllVehicles, estimatedRentalCost, makePrivateReservation, makeCorporateReservation,
//getReservByCreditCard, getAllReservsByCompany, getAllReserv, cancelReservByCreditCard, cancelReservByAcctNum
//addAccount, getAllAccounts
//******************************************************************************************
//==========================================================================================
public class SystemInterface {
    //--------------------------------------------------------------------------------------
    //Instance Variables
    //--------------------------------------------------------------------------------------
    Scanner input = new Scanner (System.in); 
    
    private static AgencyRates agency_rates;
    private static Vehicles agency_vehicles;
    private static CorporateAccounts accounts;
    
    //--------------------------------------------------------------------------------------
    //initSystem
    //--------------------------------------------------------------------------------------
    //Called by main. Creates agency_rates, agency_vehicles, and accounts objects. 
    //Initializes list of "must use" vehicles. 
    public static void initSystem(){
        agency_rates = new AgencyRates();
        agency_vehicles = new Vehicles();
        accounts = new CorporateAccounts();
        Utilities.initUtilities(accounts);
      
        //InitVehicles
        Vehicle ford_fusion = new Car("Ford Fusion", 34, "4", "AB4FG5689GM");
        Vehicle dodge_caravan = new SUV("Dodge Caravan", 25, "2", "QK3FL4273ME");
        Vehicle ten_foot_truck = new Truck("Ten-Foot Truck", 12, "1", "EJ5KU2435BC");
        agency_vehicles.add(ford_fusion);
        agency_vehicles.add(dodge_caravan);
        agency_vehicles.add(ten_foot_truck);
        
        //Init fake account
        String acct_num;
        acct_num = accounts.add("Nick", true); 
        System.out.println("Test sign_in: " + acct_num);
        
    }//Close initSystem
    
    //--------------------------------------------------------------------------------------
    //initUtil
    //--------------------------------------------------------------------------------------
    //gives Utilities class access to CorporateAccounts information 
    public static void initUtil () {
        Utilities.initUtilities(accounts);
    }//Close initUtil
    
    //--------------------------------------------------------------------------------------
    //getCarRates
    //--------------------------------------------------------------------------------------
    public static String[] getCarRates(boolean discount){
        Rates r = agency_rates.getCarRate();
        String[] display_lines = new String[1];
        if (discount) {
            double insur = r.getDailyInsur() - (r.getDailyInsur() * 0.15); 
            double mile = r.getPerMile()- (r.getPerMile() * 0.15); 
            double day = r.getPerDay() - (r.getPerDay() * 0.15); 
            double week = r.getPerWeek() - (r.getPerWeek() * 0.15); 
            double month = r.getPerMonth() - (r.getPerMonth() * 0.15); 
           // String.format( "%.2f, %.2f, %.2f, %.2f, %.2f", insur, mile, day, week, month);
            display_lines[0] = String.format("Daily      Weekly      Monthly        Per Mile        Daily Insurance\n%.2f, %10.2f, %10.2f, %11.2f, %15.2f", day, week, month, mile, insur);  
            return display_lines; 
        }//Close if
        
        display_lines[0] = r.toString(); 
      
        return display_lines;
    }//Close getCarRates
    
    //--------------------------------------------------------------------------------------
    //getSUVRates
    //--------------------------------------------------------------------------------------
    public static String[] getSUVRates(boolean discount){
        Rates r = agency_rates.getSUVRate();
        String[] display_lines = new String[1];
        if (discount) {
            double insur = r.getDailyInsur() - (r.getDailyInsur() * 0.15); 
            double mile = r.getPerMile()- (r.getPerMile() * 0.15); 
            double day = r.getPerDay() - (r.getPerDay() * 0.15); 
            double week = r.getPerWeek() - (r.getPerWeek() * 0.15); 
            double month = r.getPerMonth() - (r.getPerMonth() * 0.15); 
            
            display_lines[0] = String.format("Daily      Weekly      Monthly        Per Mile        Daily Insurance\n%.2f, %10.2f, %10.2f, %11.2f, %15.2f", day, week, month, mile, insur);  
            return display_lines; 
        }//Close if
        display_lines[0] = r.toString(); 
           
        return display_lines;
    }//Close getSUVRates
    
    //--------------------------------------------------------------------------------------
    //getTruckRates
    //--------------------------------------------------------------------------------------
    public static String[] getTruckRates(boolean discount){
        Rates r = agency_rates.getTruckRate();
        String[] display_lines = new String[1];
        if (discount) {
            double insur = r.getDailyInsur() - (r.getDailyInsur() * 0.15); 
            double mile = r.getPerMile()- (r.getPerMile() * 0.15); 
            double day = r.getPerDay() - (r.getPerDay() * 0.15); 
            double week = r.getPerWeek() - (r.getPerWeek() * 0.15); 
            double month = r.getPerMonth() - (r.getPerMonth() * 0.15); 
            
            display_lines[0] = String.format("Daily      Weekly      Monthly        Per Mile        Daily Insurance\n%.2f, %10.2f, %10.2f, %11.2f, %15.2f", day, week, month, mile, insur);  
            return display_lines; 
        }//Close if
        display_lines[0] = r.toString();
        
        return display_lines;
    }//Close getTruckRates
    
    //--------------------------------------------------------------------------------------
    //getAvailCars
    //--------------------------------------------------------------------------------------
    public static String[] getAvailCars(){
        VehiclesIterator itr = agency_vehicles.getAvailCarsItr();
        int i = 0;
        
        String[] display_lines = new String[50];
         
        while (itr.hasNext()) {
            display_lines[i] = itr.next().toString();
            i++;
        }//Close while
         
        return display_lines;
    }//Close getAvailCars
    
    //--------------------------------------------------------------------------------------
    //getAvailSUVs
    //--------------------------------------------------------------------------------------
    public static String[] getAvailSUVs(){
        VehiclesIterator itr = agency_vehicles.getAvailSUVsItr();
        String[] display_lines = new String[50];
        int i = 0; 
        
        while (itr.hasNext()) {
            display_lines[i] = itr.next().toString();
            if (i < display_lines.length)
                    i++; 
        }//Close while
        
        return display_lines;
    }//Close getAvailSUVs
    
    //--------------------------------------------------------------------------------------
    //getAvailTrucks
    //--------------------------------------------------------------------------------------
    public static String[] getAvailTrucks(){
        VehiclesIterator itr = agency_vehicles.getAvailTrucksItr();
        String[] display_lines = new String[50];
        int i = 0; 
        
        while (itr.hasNext()) {
            display_lines[i] = itr.next().toString();
            if (i < display_lines.length)
                    i++; 
        }//Close while
        
        return display_lines;
    }//Close getAvailTruck
    
    //--------------------------------------------------------------------------------------
    //getAllVehicles
    //--------------------------------------------------------------------------------------
    public static String[] getAllVehicles(){
        VehiclesIterator itr = agency_vehicles.getAllVehiclesItr();
        String[] display_lines = new String[50];
        int i = 0; 
        
        while (itr.hasNext()) {
            display_lines[i] = itr.next().toString();
            if (i < display_lines.length)
                    i++; 
        }//Close while
        
        return display_lines;
    }//Close getAllVehicles
    
    //--------------------------------------------------------------------------------------
    //estimatedRentalCost
    //--------------------------------------------------------------------------------------
    public static String[] estimatedRentalCost(RentalDetails details) {
         
        double cost;
        String[] display_lines = new String [1];
        try { 
            Vehicle veh = agency_vehicles.getVehicle(details.getVIN());
            Rates r; 
        
            if (veh instanceof Car) { 
                 r = agency_rates.getCarRate(); 
                 Cost c = new CarCost (r.getPerDay(), r.getPerWeek(), r.getPerMonth(), r.getPerMile(), r.getDailyInsur()); 
                 veh.setCost(c); 
                 
            }//Close if
        
            else if (veh instanceof SUV) { 
                 r = agency_rates.getSUVRate();
                 Cost c = new SUVCost (r.getPerDay(), r.getPerWeek(), r.getPerMonth(), r.getPerMile(), r.getDailyInsur()); 
                 veh.setCost(c); 
                
            }//Close else if
        
            else if (veh instanceof Truck) {
                 r = agency_rates.getTruckRate(); 
                 Cost c = new TruckCost (r.getPerDay(), r.getPerWeek(), r.getPerMonth(), r.getPerMile(), r.getDailyInsur()); 
                 veh.setCost(c); 
            }//Close else if
          
            cost =  veh.calcCost(details.getDurationType(), details.getRentalFrequency(), details.getMiles(), details.getInsurOption());
            
            display_lines[0] = String.format( "Estimated Total:    $%.2f", cost );
           
        }//Close try         
        
        catch (VINNotFoundException e) { 
            display_lines[0] = ("Vehicle does not exist."); 
        }//Close catch
                
       return display_lines;
    }//Close getAvailCars
    
    //--------------------------------------------------------------------------------------
    //makePrivateReservation
    //--------------------------------------------------------------------------------------
    public static String[] makePrivateReservation(ReservationDetails details){
      
        String[] display_lines = new String [2];
        Vehicle veh;
         
        try {
            
            veh = agency_vehicles.getVehicle(details.getVIN());
            
            VehiclesIterator itr = agency_vehicles.getAllUnreservedVehiclesItr(); 
            
            while (itr.hasNext()) { 
                if (itr.next().getVIN().equals(details.getVIN())) {
      
                    agency_vehicles.getVehicle(details.getVIN()).setReserv(new PrivateCustReservation(details.getName(), details.getTimeUnit(), details.getNumTimeUnits(), details.getNumChargedTo(), details.InsuranceSelected()));
                    
            
                    Rates r; 
        
                    if (veh instanceof Car) { 
                         r = agency_rates.getCarRate(); 
                         Cost c = new CarCost (r.getPerDay(), r.getPerWeek(), r.getPerMonth(), r.getPerMile(), r.getDailyInsur()); 
                         agency_vehicles.getVehicle(details.getVIN()).setCost(c); 
                 
                    }//Close if
        
                    else if (veh instanceof SUV) { 
                        r = agency_rates.getSUVRate();
                        Cost c = new SUVCost (r.getPerDay(), r.getPerWeek(), r.getPerMonth(), r.getPerMile(), r.getDailyInsur()); 
                        agency_vehicles.getVehicle(details.getVIN()).setCost(c); 
                
                    }//Close else if
                    
                    else {
                        r = agency_rates.getTruckRate(); 
                        Cost c = new TruckCost (r.getPerDay(), r.getPerWeek(), r.getPerMonth(), r.getPerMile(), r.getDailyInsur()); 
                        agency_vehicles.getVehicle(details.getVIN()).setCost(c); 
                 
                    }//Close else
           
                    display_lines[0] = "Reservation created"; 
                    display_lines[1] = veh.getReserv().toString();
                    
                    return display_lines; 
                }//Close if
                itr.next(); 
            }//Close while
            display_lines[0] = "Vehicle is already reserved!"; 
        }//Close try 
        
        catch (VINNotFoundException e) { 
            display_lines[0] = "Error! Vehicle not found.";
            
        }//Close catch
        
       return display_lines; 
        
    }//Close makePrivateReservation
    
    //--------------------------------------------------------------------------------------
    //makeCorporateReservation
    //--------------------------------------------------------------------------------------
    public static String[] makeCorporateReservation(ReservationDetails details){
                                                   
        String[] display_lines = new String [2];
        Vehicle veh;
         
        try { 
            
            veh = agency_vehicles.getVehicle(details.getVIN());
            
            VehiclesIterator itr = agency_vehicles.getAllUnreservedVehiclesItr(); 
            
            while (itr.hasNext()) { 
                if (itr.next().getVIN().equals(details.getVIN())) {
      
                    agency_vehicles.getVehicle(details.getVIN()).setReserv(new CompanyReservation(details.getName(), details.getTimeUnit(), details.getNumTimeUnits(), details.getNumChargedTo(), details.InsuranceSelected()));
            
                    Rates r; 
        
                    if (veh instanceof Car) { 
                         r = agency_rates.getCarRate(); 
                         Cost c = new CarCost (r.getPerDay(), r.getPerWeek(), r.getPerMonth(), r.getPerMile(), r.getDailyInsur()); 
                         agency_vehicles.getVehicle(details.getVIN()).setCost(c); 
                 
                    }//Close if
        
                    else if (veh instanceof SUV) { 
                        r = agency_rates.getSUVRate();
                        Cost c = new SUVCost (r.getPerDay(), r.getPerWeek(), r.getPerMonth(), r.getPerMile(), r.getDailyInsur()); 
                        agency_vehicles.getVehicle(details.getVIN()).setCost(c); 
                
                    }//Close else if
                    
                    else {
                        r = agency_rates.getTruckRate(); 
                        Cost c = new TruckCost (r.getPerDay(), r.getPerWeek(), r.getPerMonth(), r.getPerMile(), r.getDailyInsur()); 
                        agency_vehicles.getVehicle(details.getVIN()).setCost(c); 
                 
                    }//Close else
           
                    display_lines[0] = "Reservation created"; 
                    display_lines[1] = veh.getReserv().toString();
                    
                    return display_lines; 
                }//Close if
                //itr.next(); 
            }//Close while
            display_lines[0] = "Vehicle is already reserved!"; 
        }//Close try 
        
        catch (VINNotFoundException e) { 
            display_lines[0] = "Error! Vehicle not found.";
            
        }//Close catch
        
       return display_lines; 
        
    }//Close makeCorporateReservation
    
    //--------------------------------------------------------------------------------------
    //getReservByCreditCard
    //--------------------------------------------------------------------------------------
    public static String[] getReservByCreditCard (String card_num, String vin) { 
        String[] display_lines = new String [1];
        Reservation reserv; 
                
        try { 
            Vehicle veh = agency_vehicles.getVehicle(vin);
            
            if (veh.getReserv() == null)
                display_lines [0] = "Error! Reservation does not exist";
            
            else if (veh.getReserv().getNumChargedTo().equals(card_num)) { 
                reserv = veh.getReserv(); 
                display_lines [0] = reserv.toString(); 
                return display_lines; 
            }//Close if
            
            else { 
                display_lines[0] = "Error! Reservation not found ----- Credit Card number does not match"; 
            }//Close else
        }//Close try 
        
        catch (VINNotFoundException e) { 
            display_lines [0] = "Vehicle not found"; 
        }//Close catch
   
        return display_lines; 
    }//Close getReservByCreditCard
    
    //--------------------------------------------------------------------------------------
    //getAllReservsByCompany
    //--------------------------------------------------------------------------------------
    public static String[] getAllReservsByCompany (String acct_num, String vin) { 
        String[] display_lines = new String [1];
        Reservation reserv;
        
        try { 
            Vehicle veh = agency_vehicles.getVehicle(vin);
            
            if (veh.getReserv() == null)
                display_lines [0] = "Error! Reservation does not exist";
            
            else if (veh.getReserv().getNumChargedTo().equals(acct_num)) { 
                reserv = veh.getReserv(); 
                display_lines [0] = reserv.toString(); 
                return display_lines; 
            }//Close if
            
            else { 
                display_lines[0] = "Error! Reservation not found ----- Account Number does not match"; 
            }//Close else
        }//Close try 
        
        catch (VINNotFoundException e) { 
            display_lines [0] = "Vehicle not found"; 
        }//Close catch
   
        return display_lines; 
    }//Close getReservByCreditCard

    
    //--------------------------------------------------------------------------------------
    //getAllReserv
    //--------------------------------------------------------------------------------------
    public static String[] getAllReserv () { 
        String[] display_lines = new String [50]; 
        
        VehiclesIterator itr = agency_vehicles.getReservedVehiclesItr(); 
        int i = 0; 
        
            while (itr.hasNext()) {
                if (i < 50) { 
                    display_lines[i] = itr.next().getReserv().toString(); 
                }//Close if
                i++; 
                itr.next(); 
            }//Close while
            
            return display_lines; 
 
    }//Close getAllReserv
    
    //--------------------------------------------------------------------------------------
    //cancelReservByCreditCard
    //--------------------------------------------------------------------------------------
    public static String[] cancelReservByCreditCard (String card_num, String vin) {
        
        String[] display_lines = new String [2];
                
        try { 
            Vehicle veh = agency_vehicles.getVehicle(vin); 
            if (veh.getReserv().getNumChargedTo().equals(card_num)) { 
                veh.setReserv(null); 
                display_lines[0] = "Reservation terminated"; 
            }//Close if  
            
            else { 
                display_lines[0] = "Error! Reservation not found"; 
            }//Close else
        }//Close try
       
        catch (VINNotFoundException e) { 
            System.out.println ("Vehicle not found"); 
        }//Close catch 
            
            
        return display_lines; 
    }//Close getReservByCreditCard

    //--------------------------------------------------------------------------------------
    //cancelReservByAcctNum
    //--------------------------------------------------------------------------------------
    public static String[] cancelReservByAcctNum (String acct_num, String vin) { 
        String[] display_lines = new String [2];
        
        try { 
        
            Vehicle veh = agency_vehicles.getVehicle(vin); 
        
            if (veh.getReserv().getNumChargedTo().equals(acct_num)) {
                veh.setReserv(null);
                display_lines[0] = "Reservation terminated"; 
            }//Close if
            
            else { 
                display_lines[0] = "Error! Reservation not found"; 
            }//Close else
            
        }//Close try 
        
        catch (VINNotFoundException e) { 
            System.out.println ("Vehicles not found"); 
        }//Close catch 
        
        return display_lines; 
                         
    }//Close cancelReservByAcctNum
    
    //--------------------------------------------------------------------------------------
    //addAccount
    //--------------------------------------------------------------------------------------
    public static String [] addAccount (Account acct_info) {
        
        String[] display_lines = new String [4];
        String acct_num; 
        
        acct_num = accounts.add(acct_info.getName(), acct_info.insuranceDesired());
        
        display_lines[0] = "Account added"; 
        display_lines[1] = "Name: " + acct_info.getName();
        display_lines[2] = "Insurance: " + acct_info.insuranceDesired(); 
        display_lines[3] = "Account Number: " + acct_num; 
        return display_lines; 
    }//Close addAccount
    
    //--------------------------------------------------------------------------------------
    //getAllAccounts
    //--------------------------------------------------------------------------------------
    public static String [] getAllAccounts () { 
         String[] display_lines = new String [50];
         int i = 0; 
         
         Utilities.initUtilities(accounts); 
         AccountsIterator itr = accounts.getAllAccountsItr(); 
         
         while (itr.hasNext()) { 
             if (i < 50) { 
                 display_lines[i] = itr.next().toString(); 
             }//Close if
             i++;
              
         }//Close while
         
        return display_lines; 
    }//Close getAllAccounts
    //----------------------------------------------------------------------------------------
    
    public static String[] corporateLogin() {
        String[] display_lines = new String [50]; 
        Utilities.initUtilities(accounts);
        AccountsIterator itr = accounts.getAllAccountsItr();
        int i = 0; 
        
        while (itr.hasNext()) { 
            if (i < 50) { 
                 display_lines[i] = itr.next().getAcctNum(); 
             }//Close if
             
             itr.next(); 
             i++; 
        }//Close while
            
        return display_lines;
    }//Close corporateLogin
    //-----------------------------------------------------------------------------------------
    //Upon login, the corporateInfo class is called to find the name and insur option for the given 
    //account.
    public static Account corporateInfo(String num) {
        
            //AccountsIterator itr = accounts.getAllAccountsItr(); 
            Account acct; 
            
            try { 
                 Utilities.initUtilities(accounts);
                 acct = accounts.getAccount(num);  
            }//Close try
            
            catch (AccountException e) { 
                System.out.println ("Account not est"); 
                acct = null; 
            }//Close catch
            
            return acct; 
    }//Close corporateInfo class
}//Close SystemInterface Class
