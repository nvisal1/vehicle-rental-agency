//------------------------------------------------------------------------------------------
// Program 3 - Vehicle Rental Agency
// Nicholas Visalli 
// COSC 237.180
// Spring 2017
//------------------------------------------------------------------------------------------
package program3.vehicle.rental.agency;
//==========================================================================================
// Interface UserInterface
//------------------------------------------------------------------------------------------
//UserInterace is implemented by PrivateCustomerUI, CorporateUserInterface, 
//and ManagerUserInterface
//******************************************************************************************
//List of methods: starts
//******************************************************************************************
//==========================================================================================
public interface UserInterface {
    //--------------------------------------------------------------------------------------    
    //Interface Methods 
    //--------------------------------------------------------------------------------------
    public void start(); 
//------------------------------------------------------------------------------------------ 
}//Close UserInterface Interface 
